sfs_MaxFires = 75
--Maximum Number Of Fires That Can Spawn
sfs_DarkRPEnabled = true
--Set To False If You Aren't Using DarkRP
sfs_ExtinguishPay = 60
--Amount Of Money Given To A Player That Extinguishes A Fire
sfs_FirePlayerDamage = 5
--Amount Of Damage The Player Recieves If The Become Too Close To The Fire
sfs_ExtinguishDamage = 2
--Amount Of Damage The Players Does To The Fire To Extinguish The Fire
sfs_HoseDamage = 5
--Amount Of Damage The Players Does To The Fire To Extinguish The Fire With Hose
sfs_FireSpreadTick = 25
--Amount Of Time It Takes For A Fire To Spread / Decreases As Number Of Fires Increases

sfs_FireTrucks = {"scaniafiretdm"}
--List Of Vehicles That Can Fill Up Extinguisher / Use Hose On

sfs_Materials = {MAT_DIRT, MAT_WOOD, MAT_COMPUTER, MAT_FOLIAGE, MAT_PLASTIC, MAT_SAND, MAT_SLOSH, MAT_TILE, MAT_GRASS, MAT_VENT}
--List Of Materials That The Fire Can Spread To
