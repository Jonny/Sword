sfs_Fires = {}
local Fire_Location = {
	Vector(-3491, -7276, 262),
	Vector(5575, -3554, 292),
	Vector(-1351, -7868, 125),
	Vector(-7337, -4554, 136),
	Vector(-7630, -7200, 136),
	Vector(7948, 2896, 64),
	Vector(10795, 13446, 122),
	Vector(2186, 6232, 132),
	Vector(7018, -3012, 128),
	Vector(-13515, 14273, 276)
}
local Fire_StartText = "A Report Has Come Through"
local Fire_Text = {
	"Of A Microwave Setting On Fire Inside The Fire Station (You Pleb)",
	"That A Vehicle Has Exploded Inside The Car Dealer",
	"That Someone Had A Bad Day Making Meth And It Exploded",
	"Of An ATM Catching Fire As Someone Tried Inserting Their Tie Into The Machine",
	"Of A Man Thowing A Molotov Into The Bank",
	"That The Sun Has Heated The Ground Too Much That The Grass Has Caught Fire",
	"Of A Woman Shooting An Explosive Barrel To Impress Her Friends",
	"Of An Oil Spill Setting Fire Due To The Sun's Ultra Death Ray",
	"Of A Firework Exploding Up Someone's Anus And Setting Fire To The Ground",
	"Of A Youth Camp Bonfire Going Out Of Control"
}
function CreateRandomFire()
	if (sfs_DarkRPEnabled == true) then
		local Chance = math.random(0, 25)
		if (Chance == 0) then
			local NumberOfFires = 0
			for k, v in pairs(sfs_Fires) do
				if (v:IsValid()) then
					NumberOfFires = NumberOfFires + 1
				end
			end
			if (NumberOfFires >= sfs_MaxFires) then return end
			local Destination = math.random(1, table.Count(Fire_Location))
			local NewText = StartText.." "..Fire_Text[Destination]
			local NewFire = ents.Create("fire")
			NewFire:SetPos(Fire_Location[Destination])
			NewFire:Spawn()
			for k, v in pairs(player:GetAll()) do
				if(v:Team() == TEAM_FIREFIGHTER) then
					DarkRP.notify(v, 1, 4, NewText)
				end
			end
			return
		end
	end
end
timer.Create("Timer", 25, 0, CreateRandomFire)
