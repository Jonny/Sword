AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()

	self:DrawShadow(false)
	self.LastFireSpread = CurTime()
	self.ExtinguisherLeft = 100
	table.insert(sfs_Fires, self)

end

function ENT:KillFire()
	self:StopSound("ambient/alarms/combine_bank_alarm_loop4.wav")
	table.remove(sfs_Fires, self)
	self:Remove()
end

function ENT:Hurt(ply, damage)
	self.ExtinguisherLeft = self.ExtinguisherLeft - damage
	if (self.ExtinguisherLeft <= 0) then
		self:KillFire()
		if (sfs_DarkRPEnabled == true) then
			ply:addMoney(sfs_ExtinguishPay)
			DarkRP.notify(ply, 2, 5,  "You've Been Given $"..sfs_ExtinguishPay.." For Extinguishing A Fire")
		end
	end
end

function ENT:WithinRadius(ent, size)
	if (ent:GetClass() == "fire") then return false end
	local firevector = self:GetPos()
	local entvector = ent:GetPos()
	local firex = firevector.x
	local firey = firevector.y
	local firez = firevector.z
	local entx = entvector.x
	local enty = entvector.y
	local entz = entvector.z
	if (math.abs(firex - entx) > size) then return false end
	if (math.abs(firey - enty) > size) then return false end
	if (math.abs(firez - entz) > size) then return false end
	return true
end

function ENT:SpreadFire()
	for i = 1, 20 do
		local trace = {}
		trace.start = self:GetPos() + Vector( 0, 0, 10 )
		trace.endpos = self:GetPos() + Vector( math.Rand( -1, 1 ) * math.Rand( 50, 100 ), math.Rand( -1, 1 ) * math.Rand( 50, 100 ), 50 )
		trace.filter = self.Entity
		trace.mask = MASK_OPAQUE
		
		local traceline1 = util.TraceLine( trace )
	
		local trace2 = {}
		trace2.start = self:GetPos() + Vector( math.Rand( -1, 1 ) * math.Rand( 50, 100 ), math.Rand( -1, 1 ) * math.Rand( 50, 100 ), 50 )
		trace2.endpos = self:GetPos() + Vector( 0, 0, 10 )
		trace2.filter = self.Entity
		trace2.mask = MASK_OPAQUE
			
		local traceline2 = util.TraceLine( trace2 )
		
		if ( traceline1.Hit and traceline2.Hit ) or ( not traceline1.Hit and not traceline2.Hit ) then
		
			local trstart = self:GetPos() + Vector( math.Rand( -1, 1 ) * math.Rand( 50, 100 ), math.Rand( -1, 1 ) * math.Rand( 50, 100 ), 50)
			local trend = trstart - Vector( 0, 0, 100 )
			
			local trace = {}
			trace.start = trstart
			trace.endpos = trend
			trace.filter = self.Entity
				
			local TraceResults = util.TraceLine( trace )
				
			if TraceResults.HitWorld then
				if util.IsInWorld( TraceResults.HitPos ) then
					if table.HasValue( sfs_Materials, TraceResults.MatType ) then
									
						local NearOtherFire = false
						
						for k, v in pairs( ents.FindInSphere( TraceResults.HitPos, 50) ) do
							if v:GetClass() == "fire" then
								NearOtherFire = true
							end
						end
						
						if !NearOtherFire then
							local NumberOfFires = 0
							for k, v in pairs(sfs_Fires) do
								if (v:IsValid()) then
									NumberOfFires = NumberOfFires + 1
								end
							end
							if (NumberOfFires >= sfs_MaxFires) then return end
							local NewFire = ents.Create( "fire" )
							NewFire:SetPos( TraceResults.HitPos )
							NewFire:Spawn()
							return
						end
					end
				end
			end
		end
	end
end

function ENT:SpreadFireAlt()
	local Chance = math.random(0, 25)
	if (Chance == 0) then
		local vector = self:GetPos()
		local x = vector.x
		local y = vector.y
		local z = vector.z
		local radiusmin = 50
		local radiusmax = 100
		local newx = x + math.random(radiusmin, radiusmax) * (2 * (math.random(0, 1) - 0.5))
		local newy = y + math.random(radiusmin, radiusmax) * (2 * (math.random(0, 1) - 0.5))
		local newz = z + math.random(radiusmin, radiusmax) * (2 * (math.random(0, 1) - 0.5))
		local newvector = Vector(newx, newy, z)
		local NewFire = ents.Create("fire")
		NewFire:SetPos(newvector)
		NewFire:Spawn()
		for k, v in pairs(ents.GetAll()) do
			if (self:WithinRadius(v, 50)) then
				if (v:GetClass() == "fire") then
					if (NewFire == v) then return end
					NewFire:KillFire()
				end
			end
		end
	end
end

function ENT:Think()
	if self:WaterLevel() > 0 then 
		self:KillFire() 
		return 
	end
	if (self.LastFireSpread + sfs_FireSpreadTick < CurTime()) then
		self.LastFireSpread = CurTime()
		self:SpreadFire()
	end
	for k, v in pairs(ents.GetAll()) do
		if (self:WithinRadius(v, 50)) then
			if (v:IsPlayer() and v:Alive()) then
				v:TakeDamage(sfs_FirePlayerDamage, v)
			elseif (v:GetClass() == "prop_physics") then
				v:Ignite(60)
				v:SetColor(Color(136, 45, 23, 255))
				timer.Simple(60, function()
					if (v:IsValid()) then
						if (v:IsOnFire()) then
							Entity:Remove() 
						end
					end
				end)
			end
		end
	end
end
