if SERVER then
	AddCSLuaFile("shared.lua")
end

if CLIENT then
	SWEP.PrintName = "Fire Axe"
	SWEP.Slot = 2
	SWEP.SlotPos = 1
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
end

SWEP.Author = "Sword" 
SWEP.Instructions = "Use The Fire Axe To Break Down Doors"
SWEP.Category = "Sword's Fire System"

SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false

SWEP.Spawnable = true
SWEP.AdminOnly = true
SWEP.UseHands = true
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = ""

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = ""

SWEP.ViewModel = "models/weapons/v_fireaxe.mdl"
SWEP.WorldModel = "models/weapons/w_fireaxe.mdl"

function SWEP:Initialize()
	self:SetWeaponHoldType("melee")
end

function SWEP:CanPrimaryAttack() return true end

function SWEP:PrimaryAttack()	
	self.Owner:SetAnimation( PLAYER_ATTACK1 )
	self.Weapon:EmitSound( "npc/vort/claw_swing" .. math.random( 1, 2 ) .. ".wav" )
	self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
	
	self.Weapon:SetNextPrimaryFire( CurTime() + 0.5 )
	
	local tr = self.Owner:GetEyeTrace()
	local ent = tr.Entity
	
	if self.Owner:EyePos():Distance( tr.HitPos ) > 75 then 
		return 
	end
	
	if tr.MatType == MAT_GLASS then
		self.Weapon:EmitSound("physics/glass/glass_cup_break" .. math.random( 1, 2 ) .. ".wav")
		return
	elseif tr.HitWorld then
		self.Weapon:EmitSound("physics/metal/metal_canister_impact_hard" .. math.random( 1, 3 ) .. ".wav")
		return
	end
	
	if ent:IsPlayer() or ent:IsNPC() then
		self.Weapon:EmitSound("physics/flesh/flesh_impact_hard" .. math.random( 1, 6 ) .. ".wav")
		
		if SERVER then
			ent:TakeDamage( FIREAXE_PlayerDamage )
		end
	elseif ent:isDoor() then
		self.Weapon:EmitSound("physics/wood/wood_box_impact_hard" .. math.random( 1, 3 ) .. ".wav")
		
		local NearFire = false
		for k, v in pairs(ents.FindInSphere(ent:GetPos(), FIREAXE_FireRange)) do
			if v:GetClass() == "fire" then
				NearFire = true
			end
		end
		
		if NearFire then
			ent.DoorHealth = ent.DoorHealth or math.random( 4, 6 )
			ent.DoorHealth = ent.DoorHealth - 1
			
			if ent.DoorHealth <= 0 then
				ent.DoorHealth = nil
				
				if SERVER then
					ent:Fire("unlock", "", 0)
					ent:Fire("open", "", 0.5)
				end
			end
		else
			if SERVER then
				DarkRP.notify(self.Owner, 1, 5,  "There is no fire nearby.")
			end
		end
	elseif ent:GetClass() == "func_breakable_surf" then
		ent:Fire( "Shatter" )
		ply:EmitSound( "physics/wood/wood_box_impact_hard" .. math.random( 1, 3 ) .. ".wav" )
	elseif ent.fadeActivate then
		local NearFire = false
		for k, v in pairs(ents.FindInSphere(ent:GetPos(), FIREAXE_FireRange)) do
			if v:GetClass() == "fire" then
				NearFire = true
			end
		end
		
		if NearFire then
			if not ent.fadeActive then
				local FadeRandomize = math.random( 1,3 )

				if tonumber( FadeRandomize ) == 2 then
					ent:fadeActivate()
					timer.Simple( 5, function() 
						if IsValid( ent ) and ent.fadeActive then 
							ent:fadeDeactivate() 
						end 
					end )
					self.Owner:EmitSound( "physics/wood/wood_box_impact_hard" .. math.random( 1, 3 ) .. ".wav" )
				else
					if SERVER then
						DarkRP.notify(self.Owner, 1, 5, "You've failed to ram the faded door!")
						return
					end
				end
			end
		else
			if SERVER then
				DarkRP.notify(self.Owner, 1, 5,  "There is no fire nearby.")
			end
		end
	end
end

function SWEP:SecondaryAttack()
end